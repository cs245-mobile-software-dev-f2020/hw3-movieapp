package com.example.moviedb

import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.http.GET
import retrofit2.http.Query

class APIManager(val viewModel: MovieViewModel) {
    private val apiURL = "https://api.themoviedb.org/"
    private val page = 1
    private val apiKey = "d18dc82a4d9696877f9cd18235c569c9"


    fun loadJSON(str: String): ArrayList<Movie> {
        val data = JSONObject(str)
        val dataArray = data.getJSONArray("results")
        val movieList = ArrayList<Movie>()
        for (i in 0 until dataArray.length()) {
            val m = Movie()
            val movieObject = dataArray.getJSONObject(i)
            m.id = movieObject.getString("id")
            m.title = movieObject.getString("title")
            m.poster_path = movieObject.getString("poster_path")
            m.vote_average = movieObject.getDouble("vote_average")
            m.overview = movieObject.getString("overview")
            m.release_date = movieObject.getString("release_date")
            movieList.add(m)
        }
        return movieList
    }

    fun fetchRecentMovie(movie_type: String) {
        val retrofit = Retrofit.Builder()
            .baseUrl(apiURL)
            .build()
        val service = retrofit.create(MovieService::class.java)

        if (movie_type == "nowplaying") {
            val call = service.getNowPlayingMovies(apiKey, page)
            call.enqueue(MovieCallback())
        } else {
            val call = service.getUpcomingMovies(apiKey, page)
            call.enqueue(MovieCallback())
        }
    }

    inner class MovieCallback() : Callback<ResponseBody> {
        override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
            println("failed")
            println(t.message)
        }

        override fun onResponse(
            call: Call<ResponseBody>,
            response: Response<ResponseBody>
        ) {
            if (response.isSuccessful) {
                response.body()?.let {
                    val movies = loadJSON(it.string())
                    viewModel.updateList(movies)
                }
            }
        }
    }

    interface MovieService {
        @GET("3/movie/now_playing?")
        fun getNowPlayingMovies(
            @Query("api_key") api_key: String,
            @Query("page") page: Int
        ): Call<ResponseBody>

        @GET("3/movie/upcoming?")
        fun getUpcomingMovies(
            @Query("api_key") api_key: String,
            @Query("page") page: Int
        ): Call<ResponseBody>
    }
}