package com.example.moviedb


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.observe
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.fragment_list.*


/**
 * A simple [Fragment] subclass.
 */
class ListFragment : Fragment() {

    val viewModel: MovieViewModel by activityViewModels<MovieViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        return inflater.inflate(R.layout.fragment_list, container, false)
    }

    lateinit var recyclerViewAdapter: RecyclerViewAdapter
    lateinit var recyclerViewManager: LinearLayoutManager

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        bottom_navigation.setOnNavigationItemSelectedListener {
            if (it.itemId == R.id.action_nowplaying) {
                viewModel.fetchRecentMovie("nowplaying")
            }
            if (it.itemId == R.id.action_upcoming) {
                viewModel.fetchRecentMovie("upcoming")
            }
            true
        }

        recyclerViewManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        recyclerViewAdapter = RecyclerViewAdapter(emptyArray(), click = {
            viewModel.showCurrentMovie(it)
            findNavController().navigate(R.id.action_listFragment_to_detailFragment)
        })

        list_recyclerView.apply {
            layoutManager = recyclerViewManager
            adapter = recyclerViewAdapter
        }

        viewModel.movieList.observe(viewLifecycleOwner) {
            recyclerViewAdapter.movieList = it
            recyclerViewAdapter.notifyDataSetChanged()
        }
    }
}
