package com.example.moviedb

import androidx.core.graphics.rotationMatrix
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "savedMovies")
class Movie {

    @PrimaryKey
    var id = ""

    var poster_path = ""

    var title = "Movie"

    var vote_average = 0.0

    var overview = "Overview"

    var release_date = "2019-10-04"

    var saved_flag = 0

    var star = 0

}