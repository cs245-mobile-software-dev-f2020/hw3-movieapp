package com.example.moviedb

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface MovieDAO {
    @Query("SELECT * FROM savedMovies")
    fun getAll(): List<Movie>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(movie: Movie)

    @Query("DELETE FROM savedMovies WHERE id=:sid")
    fun deleteMovieById(sid: String)

    @Query("SELECT star FROM savedMovies WHERE id=:movie_id")
    fun checkExist(movie_id: String): List<Int>
}