package com.example.moviedb

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class MovieViewModel : ViewModel() {
    fun fetchRecentMovie(movie_type: String) {
        apiManager.value?.fetchRecentMovie(movie_type)
    }

    var movieList = MutableLiveData<Array<Movie>>()
    var currentMovie = MutableLiveData<Movie>()
    var apiManager = MutableLiveData<APIManager>()

    var database = MutableLiveData<MovieDB>()

    init {
        movieList.value = emptyArray()
        apiManager.value = APIManager(this)
    }

    fun showCurrentMovie(movie: Movie) {
        val like = database.value?.movieDAO()?.checkExist(movie_id = movie.id)
        if (!like.isNullOrEmpty()) {
            movie.star = like.get(0)
        }
        currentMovie.postValue(movie)
    }

    fun updateList(movies: ArrayList<Movie>) {
        movieList.value = movies.toTypedArray()
        updateList()
    }

    fun updateList() {
        val dbmovies = database.value?.movieDAO()?.getAll()
        movieList.value?.forEach { m ->
            val matched = dbmovies?.find {
                it.id == m.id
            }
            matched?.let {
                m.star = it.star
            }
        }
        movieList.value?.sortByDescending { it.star }
        movieList.postValue(movieList.value)
    }

    fun likeCurrentMovie(star: Int) {
        currentMovie.value?.let {
            it.star = star
            database.value?.movieDAO()?.insert(it)
            currentMovie.postValue(it)
        }
        updateList()
    }
}