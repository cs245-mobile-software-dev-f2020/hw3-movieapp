package com.example.moviedb

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RatingBar
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class RecyclerViewAdapter(
    var movieList: Array<Movie>,
    val click: (Movie) -> Unit
) :
    RecyclerView.Adapter<RecyclerViewAdapter.RecyclerViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerViewHolder {
        val viewItem = LayoutInflater.from(parent.context)
            .inflate(R.layout.movie_item, parent, false)
        return RecyclerViewHolder(viewItem)
    }

    override fun getItemCount(): Int {
        return movieList.size
    }

    override fun onBindViewHolder(holder: RecyclerViewHolder, position: Int) {
        holder.bind(movieList[position], click)
    }

    class RecyclerViewHolder(val viewItem: View) : RecyclerView.ViewHolder(viewItem) {
        fun bind(m: Movie, click: (Movie) -> Unit) {
            viewItem.findViewById<TextView>(R.id.item_name).text = m.title
            val imgView = viewItem.findViewById<ImageView>(R.id.editor_poster_img)
            PosterLoader.getInstance().loadURL(m.poster_path, imgView)
            viewItem.findViewById<RatingBar>(R.id.ratingBar).rating = m.star.toFloat()

            viewItem.setOnClickListener {
                click(m)
            }
        }
    }
}