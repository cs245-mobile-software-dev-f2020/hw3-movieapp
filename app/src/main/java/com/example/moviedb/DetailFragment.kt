package com.example.moviedb


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.observe
import kotlinx.android.synthetic.main.fragment_detail.*

/**
 * A simple [Fragment] subclass.
 */
class DetailFragment : Fragment() {

    val viewModel: MovieViewModel by activityViewModels<MovieViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.currentMovie.observe(viewLifecycleOwner) {
            PosterLoader.getInstance().loadURL(it.poster_path, editor_poster_img)
            title_text.text = it.title
            release_text.text = it.release_date
            rating_text.text = "Rating:${it.vote_average}"
            overview_text.text = it.overview
            ratingBar.rating=it.star.toFloat()
        }
        ratingBar.setOnRatingBarChangeListener { ratingBar, fl, b ->
            viewModel.likeCurrentMovie(fl.toInt())
        }
    }
}
